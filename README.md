#JFormParser

JFormParser插件:根据json结构,生成页面,达到解放后端开发人员的目的,降低后端对前端开发要求,后端专心开发后台接口等服务程序,前期以表单元素为主,后期会增加更多页面元素的支持
	
	
#JFormParser依赖

目前主要依赖bootstrap css框架、jQuery两大流行核心组件,设计之初想法是降低对各个插件的依赖耦合度,构造的页面元素使用其他插件也能达到多样化可替换,使得页面效果更加丰富

##各个组件:

 - <strong>*bootstrap*</strong> : *v3.3.5*
 - <strong>*jQuery*</strong> : *1.9.1*
 - <strong>*layer*</strong> : *v2.2*
 - <strong>*WdatePicker*</strong> : *v4.8*
 - <strong>*jQWidgets*</strong> : *v3.9.1*


#JFormParser元素
	
根据众多元素,构建丰富多彩的页面,目前支持的插件如下：
	
- editor(富文本插件):富文本插件这里使用ewebeditor插件
- textarea(多行文本域):多行文本域
- select(下拉框):下拉框元素,下拉框涉及数据初始化的原因,所以插件提供了remote_url属性通过后台加载数据初始化
- panel(面板):面板组件,是一个容器组件
- grid(表格):表格组件,这里的表格无任何特殊意义,仅仅只是为了页面布局,同panel一样,也是容器组件
- checkboxGroup:复选框组组件
- radioGroup:单选框组组件
- button:按钮(普通按钮、提交按钮、返回按钮、、、、等)
- buttonGroup:按钮组,是一个容器组件,包含按钮的组合,
- datagrid:表格展示组件,依赖元数据查询组件
- fileupload:文件上传
- images:图集上传组件
- bMap：地图拾取经纬度坐标组件，依赖于百度js地图(http://lbsyun.baidu.com/index.php?title=jspopular)
- text(基本文本框):文本域,文本域是很强大的一种表单元素,JFormParser目前只要支持以下几种数据类型的文本域
    - normal:常规文本域,无任务效果
    - email:只支持邮件形式的文本输入,会自检非其他格式数据
    - number:整数文本域,只支持输入整数,会自检非其他格式数据
    - decimal:小数文本域
    - datetime:日期类型,目前使用的插件是My97DatePicker日期插件,所以依赖WdatePicker.js文件



#JFormParser页面模板

目前模板主要是两个,一个是列表页模板、一个FORM表单页模板

##列表模板

json结构体示例：

    {
      "component_name":"scenic_form",
      "component_title":"景区form",
      "template_type":"list",
      "navs_title":"景区标准化管理 > 景区管理 > 景区列表",
      "navs":[{"title":"景区标准化管理","icon":"","url":""},{"title":"景区管理","icon":""},{"title":"景区列表","icon":""}],
      "resource_name":"scenic_info",
      "submit_url":"/cms/template/submit.htm",
      "childrens":[{
        "element_type":"panel",
        "element_title":"景区查询",
        "whether_header":false,
        "whether_border":false,
        "container":true,
        "childrens":[
          {
            "element_type":"datagrid",
            "element_title":"景区查询",
            "is_remote":true,
            "is_operate":true,
            "operate_title":"操作",
            "operate_buttons":[
              {"element_type":"button","type":"edit","element_title":"编辑","remote_url":"/cms/template/template_form.htm","params":{"url":"/json/baotou/scenic/scenic_form.json"}},
              {"element_type":"button","type":"delete","element_title":"图片","remote_url":"/cms/template/delete.htm"},
              {"element_type":"button","type":"delete","element_title":"视频","remote_url":"/cms/template/delete.htm"},
              {"element_type":"button","type":"delete","element_title":"音频","remote_url":"/cms/template/delete.htm"},
              {"element_type":"button","type":"delete","element_title":"删除","remote_url":"/cms/template/delete.htm"}],
            "pagination":true,
            "remote_url":"/cms/template/get_remote_list.htm",
            "columns":[
    		{"field":"title","title":"景区名称"},
    		{"field":"level","title":"景区等级"},
    		{"field":"lawyer","title":"法人代表"},
    		{"field":"person_liable","title":"负责人"},
    		{"field":"phone","title":"手机号码"},
    		{"field":"tel","title":"电话"},
    		{"field":"fax","title":"传真"},
    		{"field":"approve_date","title":"批准时间"},
    		{"field":"approve_date","title":"地理位置"}],
            "childrens":[
              {
                "element_type":"text",
                "element_title":"景区名称",
                "meta_column":"title",
                "is_query":true,
                "direction":"left",
                "width":"100%"
              },{
                "element_type":"text",
                "element_title":"景区等级",
                "meta_column":"level",
                "is_query":true,
                "direction":"left",
                "width":"100%"
              },{
                "element_type":"button",
                "element_title":"查询",
                "type":"query",
                "remote_url":"/cms/template/get_remote_list.htm",
                "params": {
                  "params": "{resource_name: scenic_info}"
                }
              },{
                "element_type":"buttonGroup",
                "element_title":"操作按钮组",
                "align":"left",
                "childrens":[
                  {"element_type":"button","type":"link","element_title":"新增景区","remote_url":"/cms/template/template_form.htm","params":{"url":"/json/baotou/scenic/scenic_form.json"}},
                  {"element_type":"button","type":"link","element_title":"删除","remote_url":"/cms/template/template_form.htm","params":{"url":"/json/baotou/scenic/scenic_form.json"}},
                  {"element_type":"button","type":"link","element_title":"导入","remote_url":"/cms/template/template_form.htm","params":{"url":"/json/baotou/scenic/scenic_form.json"}},
                  {"element_type":"button","type":"link","element_title":"导出","remote_url":"/cms/template/template_form.htm","params":{"url":"/json/baotou/scenic/scenic_form.json"}}
                ]
              }
            ]
          }
        ]
      }
      ]
    }
    
##效果如下
  ![文件列表模板效果图][1]  
##FORM表单模板

json结构体示例：

    {
      "component_name":"trips_form",
      "component_title":"行程form",
      "template_type":"form",
      "navs_title":"信息管理 > 行程管理 > 行程维护",
      "navs":[{"title":"信息管理","icon":"","url":""},{"title":"行程管理","icon":""},{"title":"行程维护","icon":""}],
      "resource_name":"trips_info",
      "submit_url":"/cms/template/submit.htm",
      "init_url":"/cms/template/get_form_data.htm",
      "childrens":[
        {
          "element_type":"panel",
          "element_title":"基础信息",
          "container":true,
          "childrens":[
            {
              "element_type":"grid",
              "cols":3,
              "rows":"3",
              "childrens":[{
                "element_type":"text",
                "element_title":"行程名称",
                "meta_column":"title",
                "is_required":true
              },{
                "element_type":"select",
                "element_title":"行程类型",
                "is_required":true,
                "meta_column":"type",
                "is_fk":true,
                "fk_resource_name":"scenic_spot_info",
                "fk_meta_column":"id",
                "fk_meta_column_show":"title",
                "data":[
                  {"text":"交通","value":"jt"},
                  {"text":"会议","value":"hy"},
                  {"text":"入住","value":"rz"},
                  {"text":"用餐","value":"yc"},
                  {"text":"考察","value":"kc"}
                ],
                "is_remote":false,
                "remote_url":""
              },{
                "element_type":"text",
                "element_title":"开始时间",
                "meta_column":"start_time",
                "is_required":true
              },{
                "element_type":"checkboxGroup",
                "element_title":"参与小组",
                "is_required":true,
                "meta_column":"team_infos",
                "fk_resource_name":"team_info",
                "fk_meta_column":"id",
                "fk_meta_column_show":"name",
                "is_remote":true,
                "width":"100%",
                "remote_url":"/cms/template/get_remote_data.htm"
              }]
            }
          ]
        },{
          "element_type":"panel",
          "element_title":"详情",
          "container":true,
          "childrens":[
            {
              "element_type":"editor",
              "meta_column":"intro",
              "width":"400px",
              "height":"300px"
            }
          ]
        },{
          "element_type":"buttonGroup",
          "element_title":"",
          "align":"center",
          "childrens":[
            {"element_type":"button","type":"submit","element_title":"提交","remote_url":"/cms/template/submit.htm","width":"80px","action_url":"/cms/template/template_list.htm","params":{"url":"/json/shengsi/trips/trips_list.json"}},
            {"element_type":"button","type":"link","element_title":"返回","remote_url":"/cms/template/template_list.htm","params":{"url":"/json/shengsi/trips/trips_list.json"},"width":"80px"}
          ]
        }
      ]
    }
    
##FORM模板效果如下：
![FORM模板效果如下][2]


  [1]: http://git.oschina.net/uploads/images/2017/0120/102836_fcd08910_118100.jpeg
  [2]: http://git.oschina.net/uploads/images/2017/0120/103119_723e3743_118100.jpeg